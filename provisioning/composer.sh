#!/usr/bin/env bash

# install composer system wide
/usr/local/zend/bin/php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
/usr/local/zend/bin/php -r "if (hash_file('SHA384', 'composer-setup.php') === '61069fe8c6436a4468d0371454cf38a812e451a14ab1691543f25a9627b97ff96d8753d92a00654c21e2212a5ae1ff36') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
/usr/local/zend/bin/php composer-setup.php
/usr/local/zend/bin/php -r "unlink('composer-setup.php');"

mv composer.phar /usr/local/bin/ && ln -s /usr/local/bin/composer.phar /usr/local/bin/composer
