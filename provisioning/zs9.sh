#!/usr/bin/env bash

# install zend server 9 with php7 and nginx
echo "deb http://repos.zend.com/zend-server/9.0/deb_apache2.4 server non-free" >> /etc/apt/sources.list.d/zs9.list
wget http://repos.zend.com/zend.key -O- |apt-key add -
aptitude install zend-server-nginx-php-7.0 -y

# set path to zs cli
echo "export PATH=$PATH:/usr/local/zend/bin/" >> /home/vagrant/.profile
echo "export PATH=$PATH:/usr/local/zend/bin/" >> /root/.profile
ZSMANAGE="/usr/local/zend/bin/zs-manage"

# bootstrap server
$ZSMANAGE bootstrap-single-server -p admin -a True -U http://localhost:10081/ZendServer > /tmp/key.tmp.1

# store and adjust API key for further use
sed '/admin/!d' /tmp/key.tmp.1 > /tmp/key.tmp.2
sed 's/^/-N /' /tmp/key.tmp.2 > /tmp/key.tmp.3
sed 's/\t/ -K /' /tmp/key.tmp.3 > /home/vagrant/key
rm /tmp/key.*
ZSKEY=`cat /home/vagrant/key`

# create virtual host
$ZSMANAGE vhost-add -n jessie.dev -p 80 -t "`cat nginx.template`" $ZSKEY

# restart server
$ZSMANAGE restart $ZSKEY
