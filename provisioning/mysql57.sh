#!/usr/bin/env bash

# install mysql 5.7
export DEBIAN_FRONTEND=noninteractive
echo mysql-apt-config mysql-apt-config/enable-repo select mysql-5.7 | debconf-set-selections
wget https://dev.mysql.com/get/mysql-apt-config_0.8.1-1_all.deb && dpkg -i mysql-apt-config_0.8.1-1_all.deb
aptitude update && aptitude install mysql-community-server -y
