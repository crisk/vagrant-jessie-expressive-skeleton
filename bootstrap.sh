#!/usr/bin/env bash

# Author: Christian Keck <ch.keck@icloud.com>

# fix "stdin: is not a tty"
sed -i 's/^mesg n$/tty -s \&\& mesg n/g' /root/.profile

# update system
aptitude update && aptitude upgrade

# install zend server 9 on nginx with php7
sh /vagrant/provisioning/zs9.sh

# install mysql 5.7
sh /vagrant/provisioning/mysql57.sh

# install futher tools
sh /vagrant/provisioning/tools.sh

# install composer
sh /vagrant/provisioning/composer.sh

# cleanup
aptitude clean

# DONE
echo -e "\n\nFINISHED!"
echo "Your API key is stored in /home/vagrant/key ready to use for zs-manage."
echo "Added vhost on jessie.dev (please update your hosts-file)"
echo "Open your browser at: http://localhost:8000/ZendServer/"
